function mask = create_mask(c,r,sizearray)

c = (c); %+repmat([0 1],43,1);
r = median(r);

[X,Y]=meshgrid(1:sizearray(1),1:sizearray(2));

for i=1:size(c,1)
    mask_single(:,:,i) = (X-c(i,1)).^2+(Y-c(i,2)).^2<=r.^2;
end
mask = sum(mask_single,3);
return