function chrpos = find_angle_position(filename)
% returns the three string character position before the letters 'deg' in
% the filename
endpos = strfind(filename,'deg');
chrpos = endpos-3 : endpos-1;
return