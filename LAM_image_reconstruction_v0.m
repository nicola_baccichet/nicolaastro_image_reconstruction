clear all;close all;clc;
% image reconstruction script
% author: Nicola Baccichet, nicola.baccichet.12@ucl.ac.uk
% version: alpha.3
% last modified: 21/04/2015
%% load data
G = '7ap_lam';
im_fldr = '20150427_610nmFilter_7HolesSilios_Trou2mm_100-360deg/';

images_list = dir([im_fldr '*.TIF']);

cr_area = [1 1 1024 1024];      % define an area to crop if necessary, now set to include the full image
px = 13;                        % um - pixel size
L = 1024*px;                    % um - side length
x = -L/2:px:L/2-px;             % um - linear scale
%% create matrix of all images
% load all data and value of the rotation angle
char_pos = 43:45;

for i = 1:length(images_list)
    all_images(:,:,i)=imcrop(imread([im_fldr images_list(i).name]),cr_area);
    angles(i) = eval(images_list(i).name(char_pos));
end

%% display example image and FT

disp_im = double(all_images(:,:,1));
% image plot
figure,imagesc(x*1e-3,x*1e-3,disp_im)
colormap('gray')
title(['mask at ' num2str(angles(1)) ' degrees'])
xlabel('mm'),ylabel('mm')

% FFT plot
ft_im = fftshift(fft2(disp_im));
figure, imagesc(log(abs(ft_im)));
title(['FT of mask at ' num2str(angles(1)) ' degrees'])

%% load PSF for image cleaning
filename = 'computed_PSF/';                     % folder location
load([filename 'cropped_' G '_PSF.mat'],'smallPSF');  % load cropped ideal PSF computed previously
PSF = smallPSF; clear smallPSF                  % rename
%% do cleaning

% set size of arrays for convolution
[m,n] = size(disp_im);
[mb,nb] = size(PSF);
mm = m+mb-1;
nn = n+nb-1;

OTF = fft2(abs(PSF).^2,mm,nn);
OTF = fft2(abs(PSF).^2);
MTF = fftshift(abs(OTF/OTF(1,1)));    clear OTF
% set mask of 0 and 1 to use in the convolution, the region of 1 is created
% by imposing a custom limit in the condition below
mask = single(MTF>=1e-1);
% mask = abs(MTF);  % full mask - uncomment if necessary for checks

% initialise matrices
cleaned_im_FT = zeros(mm,nn,1);
id_rot_mtf = zeros(mm,nn,1);
cleaned_im = zeros(mm,nn,1);
% clean the images and stack
for i = 1:size(all_images,3)
    % align mask with corresponding angle of current image
    rotated_mask = ifftshift(imrotate(mask,angles(i),'bilinear','crop'));
    % do convolution with mask and add to previous images in the fourier
    % plane
    cleaned_im_FT = cleaned_im_FT + fft2(all_images(:,:,i),mm,nn).*rotated_mask;
    % corresponding ideal MTF for checks
    id_rot_mtf = id_rot_mtf + imrotate(MTF,angles(i),'bilinear','crop');
    % returns cleaned image
    cleaned_im = cleaned_im + fft2(all_images(:,:,i),mm,nn);

% UNCOMMENT TO STORE IMAGES FROM EVERY i STEP
%     cleaned_im(:,:,i) = fftshift(ifft2(cleaned_im_FT(:,:,i)));
%     cleaned_im(:,:,i) = abs(cleaned_im(:,:,i));
%     figure, imagesc(log(abs(fftshift(cleaned_im_FT))))
end
% clear rotated_mask
%% stack images at different angles

cleaned_sum_FT = sum(cleaned_im_FT,3);

% cleaned_sum_FT = fftshift(id_rot_mtf) .* cleaned_im;

figure,imagesc(double(nthroot(abs(fftshift(cleaned_sum_FT)),4)));
title 'Fourier transform of stacked images at focal plane'
%% perform image reconstruction

reconstr_im_FT = cleaned_sum_FT./fft2(PSF,mm,nn);
reconstr_im = (fftshift(ifft2(reconstr_im_FT)));
reconstr_im = abs(reconstr_im);

%% plot with right scale
ctr = 1;    % value for image contrast
% re-define scale for consistency
L = size(reconstr_im,1)*px;     % um - side length
x = -L/2:px:L/2-px;             % um - linear scale

figure, imagesc(x*1e-3,x*1e-3,nthroot(reconstr_im,ctr))
xlabel('mm'),ylabel('mm')
title('reconstructed image')

figure,plot(x*1e-3,reconstr_im(fix(size(reconstr_im)/2),:))
xlabel('mm'),ylabel('intensity')
title('1D plot along center')