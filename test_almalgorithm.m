% image deconvolution test with set of images 0-180, based on alma pipeline
% author: Nicola Baccichet, nicola.baccichet.12@ucl.ac.uk
% version: 0.1
% last modified: 05/11/2015
%% load variable space
clear all; close all; clc;
% filename = 'test_0-180deg.mat'; filepath = '/media/ucapacc/DATAPART1/PHD/Interferometers study/LAM/image_reconstruction/RECONSTRUCTION_RESULTS/27-Oct-2015/';
filename = 'test_0-180deg.mat'; filepath = 'D:/PHD/Interferometers study/LAM/image_reconstruction/RECONSTRUCTION_RESULTS/27-Oct-2015/';
load([filepath filename]);
%% setup initial vairables
% initialize variables
Irec = zeros(size(all_images(:,:,1))); % intensity recorded
D = Irec;                              % dirty beam aka PSF of system
S = Irec;                              % sampling function aka u,v coverage aka MTF

%% get visibility function
for i=1:size(all_images,3)
    % compute total visibility function
    Irec = Irec + all_images(:,:,i);
%     imagesc(Irec);axis square;truesize(gcf);drawnow
end
V = ifftshift(fft2(fftshift(Irec)));    % visibility
%% try noise variance estimation, method from:
% http://www.mathworks.com/matlabcentral/answers/106299-estimating-noise-variance-and-signal-to-noise-ratio-of-an-image
[J,noise] = wiener2(V,[3 3]);
noise = 1;
% figure, imagesc(J); truesize(gcf); axis square; colormap jet
%% compute dirty beam and sampling function
for i=1:size(all_images,3)
% for i=1:2
    % compute total D
    [s,thd] = rotate_to_center_mtfs(PSF,all_images(:,:,i),sgn);
    D = D + imrotate(PSF,sgn*thd,'nearest','crop');
%     imagesc(D);axis square;truesize(gcf);drawnow

    % compute total S
    [c2,r2,w2] = measure_circles(s,3,6,'no');
    w_im = get_weigths(all_images(:,:,i),c2);
    % creates mask with weights
    st = create_mask_withW(c2,r2,w_im,size(s));
    st = adjust_mask_center(st,size(all_images,3),c2,r2);
    S = S + st;
    imagesc(S);axis square;truesize(gcf);drawnow
end
% sampling function with weights
SW = S./noise;  % used natural weighin definition
%% compute I*P
num_FT = V.*SW;
den_FT = ifftshift(fft2(fftshift(D)));

IP_FT = num_FT./den_FT;

IP = fftshift(ifft2(ifftshift(IP_FT)));
IP = real(IP);

figure,imagesc(IP); axis square;truesize(gcf)
%% compute primary beam shape

P = airy2d(0.61,8.4);   % primary beam - with size of single aperture

I = IP.*P;

figure,imagesc(I); axis square; colormap(CubeHelix(256,0.5,-1.5,1.2,1.0)); truesize(gcf)