function [centers,radii,magnitudes] = measure_circles(MTF,mincirc,maxcirc,show)
% measures the circles and gets the variables for golay measurements
% -------------------------------------------------------------------
% variables for the measurements
% min/max circ are radius limits of circles in pixel
% mincirc = 3; maxcirc = 6; show = 'yes';

% measure the circles
[centers, radii, magnitudes] = imfindcircles(MTF,[mincirc maxcirc],...
    'Sensitivity',0.99,'Method','TwoStage','EdgeThreshold',0.05);
% plots circles on image
if strcmp(show,'yes')==1
    figure
    imagesc(MTF);
    hold on
    viscircles(centers,radii,'EdgeColor','r');
    title('MTF with uv-points detected')   
    colorbar; axis square
end

return