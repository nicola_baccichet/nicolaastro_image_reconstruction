% square visibilities analysis for a limb darkened disk
% in other words, provides the visibilites
% author: Nicola Baccichet, nicola.baccichet.12@ucl.ac.uk
% version: 1.0.3
% last modified: 12/11/2015
clear all; close all; clc;
%% load varables from reconstruction script
% filename = 'half_rot.mat';
filename = 'full_rot_p1.mat';
filepath = '/media/ucapacc/DATAPART1/PHD/Interferometers study/LAM/image_reconstruction/RECONSTRUCTION_RESULTS/09-Nov-2015/';
% subfolder = 'complex_vis_halfrot/';
subfolder = 'complex_vis_fullrot_p1/';
load([filepath filename],'px');
%% load visibilities

table_files = dir([filepath subfolder 'complex_vis*.txt']);

for i = 1:length(table_files)
% --- (not needed) to get the number from the selected file
% [~, befn]=regexp(table_files.name,'complex_vis_');
% [afn]=regexp(table_files.name,'.txt');
% filen = eval(table_files.name(befn+1:afn-1));
% ---
    allvis(:,:,i) = load([filepath subfolder table_files(i).name]);
end

% allvis = load([filepath table_files(10).name]);

%% set conversion factors

lambda = 0.61; % um
f = 43.59601e3;            % um - more realistic measurable precision
s = 206264.8/f;         % system scale in arcsec
P = 1024*px*s;          % length of ccd in arcsec
dth = px*s;             % scale unit for ccd in arcsec
df = 1/P;               % scale unit of the FT of image in arcsec

fth = -1/(2*dth) : 1/P : 1/(2*dth)-1/P;     % coordinates grid in uv plane (cyc/arcsec)

%% adjust radial measurements and adds complex visibility column

for i=1:size(allvis,3)
    all_complexI(:,i) = (allvis(:,1,i).^2 + allvis(:,2,i).^2);
    all_complexI(22,i) = all_complexI(22,i)/(49);
    all_complexI(:,i) = all_complexI(:,i)/all_complexI(22,i);
    allvis(:,3,i) = allvis(:,3,i)*df;    % convert in cyc/arcsec
    allvis(:,7,i) = all_complexI(:,i);  % complex visibilites
    ev2_no_norm = sqrt((2*allvis(:,1,i).*allvis(:,5,i)).^2 + (2*allvis(:,2,i).*allvis(:,6,i)).^2);    % errors on complex visibilities
    allvis(:,8,i) = sqrt(all_complexI(:,i).* (ev2_no_norm.^2+ev2_no_norm(22)^2));
    allvis(:,9,i) = atan2(allvis(:,2,i),allvis(:,1,i));     % phases
end

%% separate data
stp = size(allvis,1);
in = 1;
for i=1:size(allvis,3)
    v2(in:in+stp-1) = allvis(:,7,i);        % vector with all the complex visibilities
    rmeas(in:in+stp-1) = allvis(:,3,i);     % vector with all radii
    wcirc(in:in+stp-1) = allvis(:,4,i);     % vector with all weights
    ev2(in:in+stp-1) = allvis(:,8,i);       % vector with all complex vis errors
    in = in+stp;
end

% [v2,idx]=sortrows(v2,1)

%% bessel analysis - coarse - Kjetil

% set bessel equation for uniform disk
if min(rmeas)<=1e-20
    rmeas(rmeas==min(rmeas))=1e-20;
end

r = linspace(min(rmeas),max(rmeas),100);  % in (u,v) plane - cyc/arcsec

% Rsun = 1800;                         % angular diam
% Z = @(Rsun) pi*r.*Rsun;
% % compute bessel function
% bsun = abs(2*besselj(1,Z(Rsun))./Z(Rsun)).^2;
% % bsun(1) = 1;

% figure,plot(rmeas,v2,'o')
% hold on
% plot(r,bsun,'r')
% ylim([0 0.03])

%% bessel analysis from Wittkowski2001 - optimization
options = optimoptions('lsqcurvefit','TolFun',1e-30,'TolX',1e-30,'MaxFunEvals',1e4,'MaxIter',1e4);

vUD =@(th,r) abs(2*besselj(1,pi*th.*r)./(pi*th.*r)).^2;
[thUD,resnormUD,residualsUD]=lsqcurvefit(vUD,1800,rmeas,v2,1500,3000,options);

% figure,plot(r,vUD(thUD,r)); hold on; plot(rmeas,v2,'o')   
% ylim([0 0.02]); 
% title('uniform disk')
%%

vFDD=@(th,r) abs(3*sqrt(pi)*besselj(3/2,pi*th.*r)./(sqrt(2)*(pi*th.*r).^(3/2))).^2;
[thFDD,resnormFDD,residualsFDD,~,~,~,J]=lsqcurvefit(vFDD,thUD,rmeas,v2,1500,3000,options);

% figure,plot(r,vFDD(thFDD,r)); hold on; plot(rmeas,v2,'o')
% ylim([0 0.02]); 
% title('Fully darkened disk')
% xlabel('uv-Radius [cyc/arcsec]'),ylabel('Squared Visibility Amplitude')
% legend(['Fit R_{Sun} = ' num2str(round(0.5*thFDD,3)) ' [arcsec]'],'Full data points')

% beta = nlinfit(rmeas,v2,vFDD,1800);   % other method, can be more precise if required - see matlab documentation first


%% goodness of fit

R = sum(residualsFDD./ev2);
chi2 = sum(residualsFDD.^2 ./ ev2.^2);

% 95% confidence interval of the fit
ci = nlparci(thFDD,residualsFDD,'jacobian',J);


%% get value of sun disk

Rsun_meas = thFDD/2;
Rsun_lit = 959.658;
Rdiff = abs(1 - Rsun_lit/Rsun_meas);

abs(Rsun_meas-ci(1)/2)/3        % 1 sigma error
abs(Rsun_meas-Rsun_lit)/dth     % difference in arcseconds

%% replot with nice errorbars

idx = kmeans(rmeas',21);    % group by proximity

figure,plot(r,vFDD(thFDD,r),'k'); hold on;
plot(r,vUD(thUD,r),'k--')
for j=1:21
%     plot(rmeas(idx==j),v2(idx==j),'o')
    V2mean(j) = mean(v2(idx==j));
    V2std(j) = std(v2(idx==j));
    rmean(j) = mean(rmeas(idx==j));
    rstd(j) = std(rmeas(idx==j));
end
% errorbar(rmean,V2mean,V2std,'ko')
S = {'rx', 'k', 'k'};
errorbarxy(rmean,V2mean,2*rstd,2*rstd,2*V2std,2*V2std,S)
ylim([0 0.02]);
xlabel('uv-Radius [cyc/arcsec]'),ylabel('Squared Visibility Amplitude')
xlabel('uv-Radius [cyc/arcsec]'),ylabel('Squared Visibility Amplitude')
legend(['Fit R_{Sun} = ' num2str(round(0.5*thFDD,3)) '\pm' num2str(round(abs(Rsun_meas-ci(1)/2)/3,3)) ' [arcsec]'],...
    'Uniform Disk','Visibility points')
title('Fully darkened disk')


%% error propagation in scaling conversion (theoretical)

sif = f*0.01;     % um - error in focal lenght (thorlabs website: focal length tolerance = +- 1%
sipx = 1;            % um - error on px size (this is not given, but 

sis = sqrt(s^2*(sif/f)^2);              % error on the scale factor
sidth = sqrt((s*sipx)^2 + (px*sis)^2);  % error on the value of dth (angular scale unit)
siP = sidth;                            % error on the angular length value

sidf = sqrt(df^2*(siP/P)^2);            % error on scaling factor in uv plane

%% get closure phase (try)

for m=1:size(allvis,3)
cont = 0;
for k=1:14    

x(1) = allvis(cont+1,1); y(1) = allvis(cont+1,2);
x(2) = allvis(cont+2,1); y(2) = allvis(cont+2,2);
x(3) = allvis(cont+3,1); y(3) = allvis(cont+3,2);

prod123 = (x(1)+1i*y(1))*(x(2)+1i*y(2))*(x(3)+1i*y(3));

v123(k,m) = sqrt(real(prod123)^2+imag(prod123)^2);   % amplitude
phi123(k,m) = atan2(imag(prod123),real(prod123));     % phase

cont = cont+3;
end
end

figure
hist(reshape(phi123,1,14*18))