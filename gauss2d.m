% function [G,UD,B] = gauss2d(sizearray,pos,sgm,int,rUD)
% create a 2D gaussian curve - G
% create a 2D uniform disk - UD
% create a 2D airy pattern of a single aperture
%% gaussian disk
int = max(max(sun));    % intensity
sgm = 9;                % standard deviation
% pos = [514 513];        % center position - best aligned with sun image
pos = [513 513];        % center at the center

[X,Y] = meshgrid(1:1024,1:1024);
G = int*exp(-((X-pos(1)).^2+(Y-pos(2)).^2)./(2*sgm^2));

%% uniform disk
rUD = 17;
UD = (X-pos(1)).^2+(Y-pos(2)).^2<=rUD.^2;
UD = UD.*int;
%% bessel function
px = 13;        % um - pixel size
L = 1024*13;    % um - side length
M = 1024;       % number of pixels
x = -L/2:px:L/2-px;
[X1,Y1] = meshgrid(x,x);

R = sqrt(X1.^2+Y1.^2);
lambda = 0.61;   % um - wavelength
w = 8.4;         % um - single aperture diameter
z = 40e3;        % um - prop distance

B = (w^2/(lambda*z)).^2 .* (jinc(w/(lambda*z)*R)).^2;
B = B/max(max(B));