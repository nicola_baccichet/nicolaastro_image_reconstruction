% Wiener deconvolution test with set of images 0-180
% author: Nicola Baccichet, nicola.baccichet.12@ucl.ac.uk
% version: 0.0.1
% last modified: 27/10/2015
%% show some data if needed
% figure;imagesc(PSF);axis square
% %% get otf from psf
% otf = fft2(fftshift(PSF));
% otf = otf/otf(1,1); % normalized in complex plane
% %% 3D view of mtf = abs(otf)
% figure
% surf(ifftshift(abs(otf).^2))
% camlight left; lighting phong
% colormap(pmkmp(256,'IsoAZ'))
% shading flat
% %% show phase of otf
% figure
% imagesc(ifftshift(angle(otf))), axis square
% title('Phase distribution of OTF')
%% get variables for Wiener deconvolution
otf2 = zeros(size(all_images(:,:,1)));
IM = otf2; psf2 = otf2; mask2 = otf2;

for l=1:18

IM = IM + all_images(:,:,l);
% imagesc(IM); drawnow

[mtf2,thd] = rotate_to_center_mtfs(PSF,all_images(:,:,l),sgn);
psf2 = psf2 + imrotate(PSF,sgn*thd,'nearest','crop');
% imagesc(psf2),drawnow

[c2,r2] = measure_circles(mtf2,3,6,'no');
maskt = create_mask(c2,r2,size(mtf2));
maskt = adjust_mask_center(maskt,size(all_images,3),c2,r2);
mask2 = mask2 + maskt;
% imagesc(mask2),drawnow
end

otf2 = fft2(fftshift(psf2));

n = NoiseLevel(abs(otf2).^2);
%% Wiener deconvolution

num = fft2(fftshift(IM)) ;%.* conj(otf2);
den = abs(otf2).^2 + n;

rec_image = ifft2(num./den);
rec_image = ifftshift(rec_image);

%% create superimposed image
    
fim = sum(rec_image,3);

figure,imagesc(fim)