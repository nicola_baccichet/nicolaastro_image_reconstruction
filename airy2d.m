function B = airy2d(lambda,ap_diam)
% create a 2D airy pattern of a single aperture

px = 13;        % um - pixel size
M = 1024;       % number of pixels
L = M*13;    % um - side length
x = -L/2:px:L/2-px;
[X1,Y1] = meshgrid(x,x);

R = sqrt(X1.^2+Y1.^2);
% lambda = 0.61;   % um - wavelength
% w = 8.4;         % um - single aperture diameter
w = ap_diam;
z = 40e3;        % um - prop distance

B = (w^2/(lambda*z)).^2 .* (jinc(w/(lambda*z)*R)).^2;
B = B/max(max(B));  % normalize
