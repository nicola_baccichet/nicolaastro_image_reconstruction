function h = view_direction_of_plot(x,y,streched_image)
% shows chosen plot direction onto chosen image
h = figure;
imagesc(streched_image)
hold on
plot(x,y)
return