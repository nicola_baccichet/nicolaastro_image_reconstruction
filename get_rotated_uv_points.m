function MTF = get_rotated_uv_points(PSF,ang)
% MTF rotation script (using the corresponding PSF)
% author: Nicola Baccichet, nicola.baccichet.12@ucl.ac.uk
% version: 1.1
% last modified: 01/06/2015
%%
% rotate PSF
PSF = imrotate(PSF,ang,'nearest','crop');
% generate MTF
OTF = fft2(fftshift(PSF));                clear PSF
MTF = ifftshift(abs(OTF/OTF(1,1)));       clear OTF
return