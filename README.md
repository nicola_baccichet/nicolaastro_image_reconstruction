# README #

DISCLAIMER: this is a very preliminary version. A number of settings need to be changed accordingly to make it work properly.
Please contact the author for a full explanation on how to use this and what to expect.

Author: Nicola Baccichet

Date: 14 December 2015

Email: nicola.baccichet.12@ucl.ac.uk

This is a very alpha version of an image reconstruction algorithm made in MATLAB.
You will need the following toolboxes to run it properly:
image_toolbox
matlab
phased_array_system_toolbox
statistics_toolbox

### What is this repository for? ###

* Quick summary
This program allow to reconstruct interferometric images starting from a series of PSF taken at different baseline rotations.
To run properly, it needs the system's psf and a set of interferometric images of the object.
The PSF can be calculated using the script contained in the repository psf_creator (limitations applies, see repository).

The main script is called: LAM_image_reconstruction_*.m
It comes in a standalone and built-in version that can be run, either within another script, or independently.
The output images are saved in a selected folder and allows the user also to save the full workspace separately.

Included there is a separate script that can be used to measure the complex visibilities of individual images and saves them into either a .txt or .xlsx file.
Filename: uv_points_analysis.m (A)

Additional uv analysis (uniform disk and limb darkening fitting) can be done using the script: sun_disk_fit.m (requires data previously calculated with script A.

Additional descriptions are included in each script.

NB: To properly work with different interferometric systems, some constants need to be changed accordingly.

* Version
alpha.1

### How do I get set up? ###

* Configuration

1. Copy all files in a folder of preference

2. create a folder named 'RECONSTRUCTION_RESULTS' in the folder containing the scripts

3. specify within the main script (LAM_image_reconstruction_*.m) the location of the system's PSF

* Dependencies

the system PSF can be calculated using the psf_creator scripts

### Contribution guidelines ###

* Writing tests: Feel free to contact the author for a full explanation on how to setup and run the script.
* Code review: Any suggestions welcome
* Other guidelines: A demonstration on what this program produces can be found here: https://www.ucl.ac.uk/fisica-london-workshop/pdfs/abstract-6-extended

### Who do I talk to? ###

* Repo owner or admin: 

Nicola Baccichet, nicola.baccichet.12@ucl.ac.uk

* Other community or team contact