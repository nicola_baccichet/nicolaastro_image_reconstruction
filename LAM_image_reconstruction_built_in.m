function final_image = LAM_image_reconstruction_built_in(spr_path,spr_name,data_path,PSF)
% built-in image reconstruction script
% author: Nicola Baccichet, nicola.baccichet.12@ucl.ac.uk
% version: beta.11.2
% last modified: 05/11/2015
warning('off','images:initSize:adjustingMag')   % clear from warning messages of resizing
warning('off','images:imfindcircles:warnForSmallRadius')   % clear from warning messages of imfindcircles

%% load data

images_list = dir([data_path '/*.TIF']);

image_fldr_name = input('type name of folder where images will be saved: ','s');
image_folder_path = ['RECONSTRUCTION_RESULTS/' date '/images_' image_fldr_name];
mkdir(image_folder_path)

loaded_variables = load_spreadsheet_params(spr_path,spr_name,'image_reconstruction')
% G = loaded_variables.G; % this is not needed if psf is passed through
char_pos = find_angle_position(images_list(1).name);

other_parameters1 = load_spreadsheet_params(spr_path,spr_name,'PSF_crop');
other_parameters2 = load_spreadsheet_params(spr_path,spr_name,'PSF_create');

mask_intensity_limit = loaded_variables.mask_intensity_limit;   % edge limit for the mask area (adjust if needed in spreadshhet)
Npx = other_parameters1.width;
single_ap = other_parameters2.w;
lambda = other_parameters2.lambda;
cr_area = [1 1 Npx Npx];        % define an area to crop if necessary, now set to include the full image
px = other_parameters1.px;      % um - pixel size
f = other_parameters2.f;        % um - focal length
s = 206264.8/f;                 % ''/um - scale factor from um to arcsec
s = s*0.004848;                 % mrad/um
L = Npx*px;                     % um - side length
x = -L/2:px:L/2-px;             % um - linear scale
xang = x*s;                     % mrad - angular scale
% clear struct variables
clear other_parameters1 other_parameters2 loaded_variables

%% create matrix of all images
% load all data and value of the rotation angle
for i = 1:length(images_list)
    all_images(:,:,i)=imcrop(imread([data_path '/' images_list(i).name]),cr_area);
    angles(i) = eval(images_list(i).name(char_pos));
end
%% display example image and FT
% (this is purely for visualization/debugging purpose)

scrz = get(0,'screensize'); % get data on screen size

sample_plot_ctrl = input(['Type (y) to view image at ' num2str(angles(1)) ' degrees: '],'s');

if strcmp(sample_plot_ctrl,'y')==1
    disp_im = double(all_images(:,:,1));
    % image plot
    figure,imagesc(x,x,disp_im)
    truesize(gcf)
    colormap('gray')
    title(['mask at ' num2str(angles(1)) ' degrees'])
    xlabel('mm'),ylabel('mm')
    % FFT plot
    ft_im = ifftshift(fft2(fftshift(disp_im)));
    ft_im = abs(ft_im);
    figure, imagesc(log(ft_im));
    title(['FT of mask at ' num2str(angles(1)) ' degrees'])
    truesize(gcf)
end
disp('press enter to continue')
pause

% save one image and its FFT
disp_im = double(all_images(:,:,1));
figure('Visible','off'),imagesc(disp_im); axis square; colorbar
title(['mask at ' num2str(angles(1)) ' degrees']),xlabel('px'),ylabel('px')
saveas(gcf,[image_folder_path '/1_focal plane image'],'bmp');close(gcf)

ft_im = log(abs(ifftshift(fft2(fftshift(disp_im)))));
figure('Visible','off'), imagesc(ft_im), axis square; colorbar
title(['FT of mask at ' num2str(angles(1)) ' degrees'])
saveas(gcf,[image_folder_path '/2_focal plane image FFT'],'bmp');close(gcf)

%% prepare PSF for image cleaning

% get the offsets angles respect to a chosen reference, of psf and the
% first two measured images
[offsets_angles,PSF] = align_PSF(PSF,all_images(:,:,1),all_images(:,:,2));

% align PSF to first measurement
PSF = imrotate(PSF,offsets_angles(2)-offsets_angles(1),'nearest','crop');
OTF = fft2(fftshift(PSF));
MTF = ifftshift(abs(OTF/OTF(1,1))); clear OTF

% define if rotation direction is clockwise or counterclockwise
if offsets_angles(3)>offsets_angles(2)
    sgn = 1;    % counterclockwise rotation
else
    sgn = -1;   % cloclkwise rotation
end
%% save PSF and MTF

figure('Visible','off'), imagesc(PSF), axis square; colorbar
title(['PSF at ' num2str(angles(1)) ' degrees']), xlabel('px'),ylabel('px')
saveas(gcf,[image_folder_path '/3_single position PSF'],'bmp');close(gcf)

figure('Visible','off'), imagesc(MTF), axis square; colorbar
title(['MTF at ' num2str(angles(1)) ' degrees']), xlabel('px'),ylabel('px')
saveas(gcf,[image_folder_path '/4_single position MTF'],'bmp');close(gcf)

%% image reconstruction

% initialize matrices
full_uv_points = zeros(size(MTF));
full_mask = full_uv_points;
single_reconstructed_images = zeros(size(all_images));
mtf_at_different_angles = zeros(size(MTF));

% image reconstruction loop
for j = 1:size(all_images,3)
temp_image = all_images(:,:,j);

% generate MTF (uv points) at right angle - skip the first angle as this is
% already aligned
if j~=1
    mtf = get_rotated_uv_points(PSF,sgn*(abs(angles(j)-angles(1))));
    [c,r] = measure_circles(mtf,3,7,'no');
    mask = create_mask(c,r,size(mtf));
    mask = adjust_mask_center(mask,size(all_images,3),c,r);    
else
    mtf = MTF;
    [c,r] = measure_circles(mtf,3,7,'yes');
    saveas(gcf,[image_folder_path '/6_MTF with uv-points detected'],'bmp');
    mask = create_mask(c,r,size(mtf));
    mask = adjust_mask_center(mask,size(all_images,3),c,r);    

    % show circles on mask
    figure, imagesc(mask)
    hold on
    viscircles(c,r,'EdgeColor','r');
    title('Mask with uv-points'); colorbar; axis square

    % show circles on real image
    figure, imagesc(log(abs( ifftshift(ifft2(fftshift(temp_image))) )))
    hold on
    viscircles(c,r,'EdgeColor','r');
    title('Real image with MTF uv points superimposed'); colorbar; axis square
    saveas(gcf,[image_folder_path '/7_FFT of image with ideal uv-points superimposed'],'bmp');

end

% generate reconstructed image
recim = ifftshift(ifft2(fftshift(temp_image)))./mtf;
recim_mask = recim.*mask;
ftrecim = ifftshift(fft2(fftshift(recim_mask)));

% clean reconstructed image from fft residuals
ftrecim = abs(ftrecim);

% store all images in one array
single_reconstructed_images(:,:,j) = ftrecim;
mtf_at_different_angles(:,:,j) = mtf;
full_uv_points = full_uv_points+mtf;
full_mask = full_mask+mask;

% plot first reconstruction step
    if j==1
        figure, imagesc(abs(ftrecim)); title(['Reconstruced image at ' num2str(angles(j)) ' degrees']),truesize(gcf), axis square; colorbar
        saveas(gcf,[image_folder_path '/8_single reconstructed image'],'bmp');
        figure,imagesc(log(abs(recim_mask))), title('Reconstructed image in the Fourier space'), truesize(gcf), axis square; colorbar
        saveas(gcf,[image_folder_path '/9_single reconstructed image in fourier space'],'bmp');
        figure('Visible','off'),imagesc(mask), title('Single image uv-plane mask'), truesize(gcf), axis square; colorbar
        saveas(gcf,[image_folder_path '/10_single image uv-plane mask'],'bmp');
    end
end

% adjust full PSF intensity (central peak)
uv_adj = full_uv_points.*adjust_mask_center(full_mask,18,[513 513],5);
% sum all reconstructed images
final_image = sum(single_reconstructed_images,3);
% do apodization
B = airy2d(lambda,single_ap);
final_image_ap = final_image.*B;

% plot final image
figure, imagesc(abs(final_image_ap)); title('total reconstruced image'),xlabel('px'),ylabel('px'),truesize(gcf), axis square
colormap(CubeHelix(256,0.5,-1.5,1.2,1.0));
% plot uv points used
figure, imagesc(full_mask); title('total uv points used'); axis square
%% save final image, PSF and setup parameters
control_on_data_save = input('Enter (y) to save the reconstructed image in a .mat file: ','s');
if strcmp(control_on_data_save,'y')==1
    mkdir(['RECONSTRUCTION_RESULTS/' date])
    uisave
%     disp('saving variables...')
%     save([date '\' G '_reconstructed_image.mat'],'-v7.3');
    disp('Saved')
end
%% save final images
figure('Visible','off'),imagesc(final_image), title('Final reconstructed image'), truesize(gcf), axis square; colorbar
saveas(gcf,[image_folder_path '/11_final reconstructed image'],'bmp');close(gcf)
figure('Visible','off'),imagesc(final_image_ap), title('Final reconstructed image'), truesize(gcf), axis square; colorbar
colormap(CubeHelix(256,0.5,-1.5,1.2,1.0));
saveas(gcf,[image_folder_path '/12_final reconstructed image (apodized)'],'bmp');close(gcf)
figure('Visible','off'),imagesc(full_mask), title('Representation of full uv-plane coverage'), truesize(gcf), axis square; colorbar
saveas(gcf,[image_folder_path '/13_total mask'],'bmp');close(gcf)
figure('Visible','off'),imagesc(abs(ifftshift(fft2(fftshift(full_uv_points))))), title('Total PSF'), truesize(gcf), axis square; colorbar
saveas(gcf,[image_folder_path '/14_total PSF'],'bmp');close(gcf)
figure('Visible','off'),imagesc(log(abs(full_uv_points))), title('Total MTF'), truesize(gcf), axis square; colorbar
saveas(gcf,[image_folder_path '/15_total MTF'],'bmp');close(gcf)

%% convolution check - WITH REAL IMAGE
flchoice = questdlg('Click Yes to start the convolution test (with an image withouth mask)', ...
        'Flip Menu', ...
        'Yes','No','Yes');

if strcmp(flchoice,'Yes')==1
uiwait(msgbox('Select the real image file'));  % not working for version older than 2015
% get image for convolution
[sn_file,sn_path] = uigetfile('*.*','Select real image');
sun = imread([sn_path sn_file]);
%% adds a zero pad
if size(sun)<[1024 1024]
    sun = padarray(sun,[75 75]);
end
% sun FT
sunFT = ifftshift(fft2(fftshift(sun)));
% convolution with full psf
sunPSF = sunFT.*uv_adj;
% iFFT
sunconv = real(ifftshift(ifft2(fftshift(sunPSF))));
% apply apodization
sunconvap = sunconv.*B;

% show result
figure, imagesc(sunconvap); truesize(gcf);axis square;
colorbar; xlabel('px'),ylabel('px')
title('Synthetic reconstructed image')
%% save synthetic image
figure('Visible','off'),imagesc(sun),title('Image with no mask'), truesize(gcf), axis square; colorbar; xlabel('px'),ylabel('px')
saveas(gcf,[image_folder_path '/16_image with no mask'],'bmp');
figure('Visible','off'),imagesc(sunconvap),title('Synthetic reconstructed image'), truesize(gcf), axis square; colorbar; xlabel('px'),ylabel('px')
colormap(CubeHelix(256,0.5,-1.5,1.2,1.0));
saveas(gcf,[image_folder_path '/17_synthetic reconstructed image'],'bmp');
end