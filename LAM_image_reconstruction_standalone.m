% image reconstruction script
% author: Nicola Baccichet, nicola.baccichet.12@ucl.ac.uk
% version: beta.1.3
% last modified: 05/11/2015
clear all;close all;clc;
warning('off','images:initSize:adjustingMag')   % clear from warning messages of resizing
warning('off','images:imfindcircles:warnForSmallRadius')   % clear from warning messages of imfindcircles
%% load data
uiwait(msgbox('Select data folder path'));  % not working for version older than 2015
% disp('Press eneter to select data folder path');pause
im_fldr = uigetdir(pwd);  % open set folder dialog

% im_fldr = '20150427_610nmFilter_7HolesSilios_Trou2mm_100-360deg/';  char_pos = 43:45;
% im_fldr = '../tests_28_29_may2015/20150528/';  char_pos = 19:21;
% im_fldr = 'HT_measurements/';  char_pos = 39:41;
% images_list = dir([im_fldr '7Ap_300sec*300deg.TIF']);
% char_pos = 16:18;

images_list = dir([im_fldr '/*.TIF']);
char_pos = find_angle_position(images_list(1).name);
% set path for saving pictures
image_fldr_name = input('type name of folder where images will be saved: ','s');
image_folder_path = ['RECONSTRUCTION_RESULTS/' date '/images_' image_fldr_name];
mkdir(image_folder_path)

G = '7ap_lam';
cr_area = [1 1 1024 1024];      % define an area to crop if necessary, now set to include the full image
% cr_area = [151 151 874 874];
px = 13;                        % um - pixel size
f = 43.59e3;                       % um - focal length
s = 206264.8/f;                 % ''/um - scale factor from um to arcsec
convf = 0.004848;                 % mrad/um
L = cr_area(3)*px;              % um - side length
x = -L/2:px:L/2-px;             % um - linear scale
xang = x*s;                     % arcsec - angular scale
mask_intensity_limit = 35e-3;   % edge limit for the mask area (adjust if needed in spreadshhet)
P = L*s;                % lenght of CCD in arcsec
dth = px*s;             % scale unit for ccd in arcsec
df = 1/P;               % scale unit of the FT of image in arcsec
fth = -1/(2*dth) : 1/P : 1/(2*dth)-1/P;     % coordinates grid in uv plane (cyc/arcsec)
%% create matrix of all images
% load all data and value of the rotation angle
for i = 1:length(images_list)
    all_images(:,:,i)=imcrop(imread([im_fldr '/' images_list(i).name]),cr_area);
    angles(i) = eval(images_list(i).name(char_pos));
end
%% display example image and FT
% (this is purely for visualization/debugging purpose)

scrz = get(0,'screensize'); % get data on screen size

sample_plot_ctrl = input(['Type (y) to view image at ' num2str(angles(1)) ' degrees: '],'s');

if strcmp(sample_plot_ctrl,'y')==1
    disp_im = double(all_images(:,:,1));
    % image plot
    figure,imagesc(x,x,disp_im)
    truesize(gcf)
    colormap('gray')
    title(['mask at ' num2str(angles(1)) ' degrees'])
    xlabel('mm'),ylabel('mm')
    % FFT plot
    ft_im = ifftshift(fft2(fftshift(disp_im)));
    ft_im = abs(ft_im);
    figure, imagesc(log(ft_im));
    title(['FT of mask at ' num2str(angles(1)) ' degrees'])
    truesize(gcf)
end
disp('press enter to continue')
pause

% save one image and its FFT
disp_im = double(all_images(:,:,1));
figure('Visible','off'),imagesc(disp_im); axis square; colorbar
title(['mask at ' num2str(angles(1)) ' degrees']),xlabel('px'),ylabel('px')
saveas(gcf,[image_folder_path '/1_focal plane image'],'bmp');close(gcf)

ft_im = log(abs(ifftshift(fft2(fftshift(disp_im)))));
figure('Visible','off'), imagesc(ft_im), axis square; colorbar
title(['FT of mask at ' num2str(angles(1)) ' degrees'])
saveas(gcf,[image_folder_path '/2_focal plane image FFT'],'bmp');close(gcf)

%% load PSF for image cleaning
filename = '../PSF_creator/';                     % folder location
load([filename 'cropped_' G '_PSF.mat'],'smallPSF');  % load cropped ideal PSF computed previously

PSF = imcrop(smallPSF,cr_area); clear smallPSF                  % rename

% get the offsets angles respect to a chosen reference, of psf and the
% first two measured images
[offsets_angles,PSF] = align_PSF(PSF,all_images(:,:,1),all_images(:,:,2));
% change this to introduce an offset in the measurements
a = offsets_angles(1);
% align PSF to first measurement
PSF = imrotate(PSF,offsets_angles(2)-a,'nearest','crop');
OTF = fft2(fftshift(PSF));
MTF = ifftshift(abs(OTF/OTF(1,1))); clear OTF

% define if rotation direction is clockwise or counterclockwise
if offsets_angles(3)>offsets_angles(2)
    sgn = 1;    % counterclockwise rotation
else
    sgn = -1;   % cloclkwise rotation
end

%%

% for i=3:3
%    rotate_to_center_mtfs(PSF,all_images(:,:,i),sgn); 
% end

%% create apodization function 2D - NOT NEEDED

% list of window functions here
% http://uk.mathworks.com/help/signal/ref/window.html
% references here 
% http://www.mathworks.com/matlabcentral/newsreader/view_thread/35512
% ap1D = hann(size(all_images(:,:,1),1));
% ap2D = ap1D*ap1D';
% figure,imagesc(ap2D), title('apodization function')

% save PSF,MTF and ap2D images

figure('Visible','off'), imagesc(PSF), axis square; colorbar
title(['PSF at ' num2str(angles(1)) ' degrees']), xlabel('px'),ylabel('px')
saveas(gcf,[image_folder_path '/3_single position PSF'],'bmp');close(gcf)

figure('Visible','off'), imagesc(MTF), axis square; colorbar
title(['MTF at ' num2str(angles(1)) ' degrees']), xlabel('px'),ylabel('px')
saveas(gcf,[image_folder_path '/4_single position MTF'],'bmp');close(gcf)  

% figure('Visible','off'), imagesc(ap2D), axis square; colorbar
% title('Apodization function')
% saveas(gcf,[image_folder_path '/5_apodization function'],'bmp');close(gcf)

%% image reconstruction

% initialize matrices
full_uv_points = zeros(size(MTF));
full_mask = full_uv_points;
single_reconstructed_images = zeros(size(all_images));
mtf_at_different_angles = zeros(size(MTF));

% image reconstruction loop
for j = 1:size(all_images,3)
% for j = 1:1

temp_image = all_images(:,:,j);
% generate MTF (uv points) at right angle - skip the first angle as this is
% already aligned
if j~=1
    mtf = get_rotated_uv_points(PSF,sgn*(abs(angles(j)-angles(1))));
%     mtf = rotate_to_center_mtfs(PSF,temp_image,sgn);
    [c,r] = measure_circles(mtf,3,6,'no');
    mask = create_mask(c,r+1,size(mtf));
    mask = adjust_mask_center(mask,size(all_images,3),c,r+1);
%     figure,imagesc(mtf), title('mtf')       % uncomment for debug

else
    mtf = MTF;
%     mtf = rotate_to_center_mtfs(PSF,temp_image,sgn);
    [c,r] = measure_circles(mtf,3,6,'yes');
    saveas(gcf,[image_folder_path '/6_MTF with uv-points detected'],'bmp');
    mask = create_mask(c,r+1,size(mtf));
    mask = adjust_mask_center(mask,size(all_images,3),c,r+1);
    % uncomment for debug - shows circle position as well
%     figure,imagesc(mtf), title('MTF')       

%      show circles on mask
%     figure, imagesc(mask)
%     hold on
%     viscircles((c),r,'EdgeColor','r');
%     title('Mask with uv-points'); colorbar; axis square

%     show circles on real image
    figure('Visible','on'); imagesc(log(abs( ifftshift(ifft2(fftshift(temp_image))) )))
    hold on
    viscircles((c),r,'EdgeColor','r');
    title('Real image with MTF uv points superimposed'); colorbar; axis square
    saveas(gcf,[image_folder_path '/7_FFT of image with ideal uv-points superimposed'],'bmp');

end

% generate reconstructed image
recim = ifftshift(ifft2(fftshift(temp_image)))./mtf;
recim_mask = recim.*mask;
% recim_mask = recim;
ftrecim = ifftshift(fft2(fftshift(recim_mask)));
% figure;imagesc(log(abs(recim_mask))); title('Reconstructed image in the Fourier space');axis square; truesize(gcf)  % uncomment for debug

% clean reconstructed image from fft residuals
ftrecim = abs(ftrecim);

% store all images in one array
single_reconstructed_images(:,:,j) = ftrecim;
mtf_at_different_angles(:,:,j) = mtf;
full_uv_points = full_uv_points+mtf;
full_mask = full_mask+mask;

% plot single reconstructed images - uncomment for debug
% figure, imagesc(abs(ftrecim)); title(['reconstruced ' num2str(angles(j)) ' degrees image']),truesize(gcf)
    if j==1
        figure;imagesc(abs(ftrecim)); title(['Reconstruced image at ' num2str(angles(j)) ' degrees']),truesize(gcf), axis square; colorbar
        saveas(gcf,[image_folder_path '/8_single reconstructed image'],'bmp');
        figure;imagesc(log(abs(recim_mask))); title('Reconstructed image in the Fourier space');axis square; truesize(gcf); 
        saveas(gcf,[image_folder_path '/9_single reconstructed image in fourier space'],'bmp');
        figure('Visible','off');imagesc(mask), title('Single image uv-plane mask'), truesize(gcf), axis square; colorbar
        saveas(gcf,[image_folder_path '/10_single image uv-plane mask'],'bmp');
    end
end

% adjust full PSF intensity (central peak)
uv_adj = full_uv_points.*adjust_mask_center(full_mask,18,[513 513],5);
% sum all reconstructed images
final_image = sum(single_reconstructed_images,3);
% do apodization and molt for a single aperture psf
B = airy2d(0.61,8.4);
final_image_ap = final_image.*B;

% plot final image
figure, imagesc(abs(final_image_ap)); title('total reconstruced image'),xlabel('px'),ylabel('px'),truesize(gcf), axis image;  colorbar
colormap(CubeHelix(256,0.5,-1.5,1.2,1.0));
% plot uv points used
figure, imagesc(full_mask); title('total uv points used'); axis square
%% save final image, PSF and setup parameters
control_on_data_save = input('Enter (y) to save the reconstructed image in a .mat file: ','s');
if strcmp(control_on_data_save,'y')==1
    mkdir(['RECONSTRUCTION_RESULTS/' date])
    uisave
%     disp('saving variables...')
%     save([date '\' G '_reconstructed_image.mat'],'-v7.3');
    disp('Saved')
end

%% save final images
figure('Visible','off'),imagesc(final_image), title('Final reconstructed image'), truesize(gcf), axis square; colorbar; xlabel('px'),ylabel('px')
saveas(gcf,[image_folder_path '/11_final reconstructed image'],'bmp');close(gcf)
figure('Visible','off'),imagesc(final_image_ap), title('Final reconstructed image'), truesize(gcf), axis square; colorbar; xlabel('px'),ylabel('px')
colormap(CubeHelix(256,0.5,-1.5,1.2,1.0));
saveas(gcf,[image_folder_path '/12_final reconstructed image (apodized)'],'bmp');close(gcf)
figure('Visible','off'),imagesc(full_mask), title('Representation of full uv-plane coverage'), truesize(gcf), axis square; colorbar
saveas(gcf,[image_folder_path '/13_total mask'],'bmp');close(gcf)
figure('Visible','off'),imagesc(abs(ifftshift(fft2(fftshift(full_uv_points))))), title('Total PSF'), truesize(gcf), axis square; colorbar; xlabel('px'),ylabel('px')
saveas(gcf,[image_folder_path '/14_total PSF'],'bmp');close(gcf)
figure('Visible','off'),imagesc(log(abs(full_uv_points))), title('Total MTF'), truesize(gcf), axis square; colorbar
saveas(gcf,[image_folder_path '/15_total MTF'],'bmp');close(gcf)

%% convolution check - WITH REAL IMAGE
flchoice = questdlg('Click Yes to start the convolution test (with an image withouth mask)', ...
        'Flip Menu', ...
        'Yes','No','Yes');

if strcmp(flchoice,'Yes')==1
uiwait(msgbox('Select the real image file'));  % not working for version older than 2015
% get image for convolution
[sn_file,sn_path] = uigetfile('*.*','Select real image');
sun = imread([sn_path sn_file]);
%% adds a zero pad
if size(sun)<[1024 1024]
    sun = padarray(sun,[75 75]);
end
%%
% sun FT
sunFT = ifftshift((fft2(fftshift(sun))));
% sunFT = (ifftshift(sunFT/sunFT(1,1)));
% convolution with full psf
sunPSF = sunFT.*uv_adj;
% iFFT
sunconv = ifftshift(ifft2(fftshift(sunPSF)));
% apply apodization
sunconvap = abs(sunconv).*B;

%% show result
figure, imagesc(sunconvap); truesize(gcf);axis square;
colorbar; xlabel('px'),ylabel('px')
title('Synthetic reconstructed image')
%% save synthetic image
figure('Visible','off'),imagesc(sun),title('Image with no mask'), truesize(gcf), axis square; colorbar; xlabel('px'),ylabel('px')
saveas(gcf,[image_folder_path '/16_image with no mask'],'bmp');
figure('Visible','off'),imagesc(sunconvap),title('Synthetic reconstructed image'), truesize(gcf), axis square; colorbar; xlabel('px'),ylabel('px')
colormap(CubeHelix(256,0.5,-1.5,1.2,1.0));
saveas(gcf,[image_folder_path '/17_synthetic reconstructed image'],'bmp');
end
%% flux/ADU conversion

lambda = 0.61;      % um
ap = 8.4e-4;        % cm - single ap diam
gain = 1;           % CCD gain
QE = 0.96;          % CCD quantum efficiency
AR = 0.98;          % bandpass filter AR coating transmission
dlambda = 10e-9;     % filter bandwidth
t = 0.7;            % s - exposure time
r = 0.5;            % filter transmission
ext = 0.37;         % atmospheric extinition Marseilles (estimated - see notes)

% conversion factor flux/ADU - J / (cm^2 * um * s)
flux_ADU = (t*7*(pi*ap^2/4)*r*QE*gain*(1-ext)*dlambda*AR*lambda/(physconst('LightSpeed')*6.62607e-34))^-1;

%% save images at decent resolution

% % print(gcf,fullfile(pwd,'rectangle_zoomed'),'-depsc',['-r',num2str(500)],'-opengl') %save file 

%% plot entrance pupil scaled properly with filled aperture etc...
ds = 1e3;   %um
w = 8.4;    %um

c_00 = [
        0.221	-0.079;
        0.36	-0.152;
        0.385	-0.247;
        0.334	-0.344;
        0.169	-0.398;
        0.087	-0.401;
        0	0
        ];
centroid_pos = [sum(c_00(:,1))/7 ,   sum(c_00(:,2))/7];
% re centering on (0,0)
c_00 = (c_00 - repmat(centroid_pos,7,1))*ds;    % center and scale to ds units
centroid_pos = [sum(c_00(:,1))/7 ,   sum(c_00(:,2))/7];

all_dist = pdist([centroid_pos;c_00]);
D = max(all_dist)+2*w;   % um
center_D = [sum(c_00([4,7],1))/2  sum(c_00([4,7],2))/2];

th = 0:.01:2*pi;

% plot mask array
figure,plot(.5*D*cos(th)+center_D(1),.5*D*sin(th)+center_D(2),'--k','LineWidth',2)
axis equal
hold on
for j=1:size(c_00,1)
plot(w*cos(th)+c_00(j,1),w*sin(th)+c_00(j,2),'k','LineWidth',2)
end
% axis square
title('7-aperture mask')
xlabel('[um]'),ylabel('[um]')

% plot uv coverage
figure
plot((c(:,1)-513)*df,(c(:,2)-513)*df,'k+'), axis square;
axis([-1/(2*dth) 1/(2*dth)-1/P -1/(2*dth) 1/(2*dth)-1/P])
xlabel('u [cyc/arcsec]'), ylabel('v [cyc/arcsec]')
title('uv coverage')
xlim([-5e-3 5e-3]),ylim([-5e-3 5e-3])