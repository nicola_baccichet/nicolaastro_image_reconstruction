function [newcoord]=rotation_matrix(theta,oldcoord)
% rotation matrix script
% author: Nicola Baccichet, nicola.baccichet.12@ucl.ac.uk
% version: 1
% last modified: 13/04/2015

% oldcoord intended to be a column vector

oldcoord = oldcoord';

t = deg2rad(theta);
R = [
        cos(t)      -sin(t);
        sin(t)      cos(t)
    ];

newcoord = R*oldcoord;

newcoord = newcoord';
return