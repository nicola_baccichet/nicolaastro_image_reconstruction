function zero_angle = findZeroMTF(mtf,starting_angle)
% optimizes angles of MTF alignment (found manually) within a 2 degrees
% span from the starting angle.
% author: Nicola Baccichet, nicola.baccichet.12@ucl.ac.uk
% version: 1.0.1
% last modified: 26/10/2015
%%
stp = 0.01; % step angle
angles = (starting_angle-2):stp:(starting_angle+2);  % range of angles to check - degrees
c = size(mtf)/2+[1 1];  % image center
for i = 1:length(angles)
% define direction
a = -tan(deg2rad(angles(i)));
x = 400:size(mtf,1)-400;
y = round(a.*(x-c(1))+c(2));
% get data along defined line
ind = sub2ind(size(mtf),y,x);
%plot
% view_direction_of_plot(x,y,mtf); truesize(gcf)
% store line to check
line_to_check = mtf(ind);
% figure,plot(line_to_check);
value_of_line(i) = trapz(line_to_check);
end

% figure,plot(angles,value_of_line)
max(value_of_line);
zero_angle = angles(value_of_line==max(value_of_line));
if length(zero_angle)~=1
    zero_angle = mean(zero_angle);
end