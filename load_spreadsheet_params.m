function cell_variables = load_spreadsheet_params(spr_path,spr_name,spr_sheet)
% load parameters for the image reconstruction scripts.
% the parameters are loaded from the sheet specified by 'spr_sheet' inside
% the xls file specified by 'spr_name'
% author: Nicola Baccichet, nicola.baccichet.12@ucl.ac.uk
% version: 1.1
% last modified: 12/06/2015

% set file and path
filename = [spr_path '/' spr_name];
% show which file is selected
struct('filename',spr_name,'sheet',spr_sheet)
% load selected parameters set into a struct array
switch spr_sheet
    case 'PSF_create'
        sheet = 1;
        xlRange = 'A3:F3';
        [nmb,str] = xlsread(filename,sheet,xlRange);
        cell_variables = struct('lambda',nmb(1),'f',nmb(2),...
            'zxp',nmb(3),'w',nmb(4),'ds',nmb(5),'G',str);
    case 'PSF_crop'
        sheet = 2;
        xlRange = 'A3:D3';
        [nmb,str] = xlsread(filename,sheet,xlRange);
        cell_variables = struct('width',nmb(1),'height',nmb(2),'px',nmb(3),'D',str);
    case 'image_reconstruction'
        sheet = 3;
        xlRange = 'A2:D2';
        [nmb,str] = xlsread(filename,sheet,xlRange);
        cell_variables = struct('theta_pos_start',nmb(1),'theta_pos_end',nmb(2),...
            'G',str,'mask_intensity_limit',nmb(4));
end