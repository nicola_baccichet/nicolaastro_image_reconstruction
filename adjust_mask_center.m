function newmask = adjust_mask_center(oldmask,N,c,r)
% select central peak of mask and lowers its value to 1/N

% oldmask=mask;

% coordinate and radius of the center
cm_coord = fix(c(1,:));
rad_px = r(1)+5;

% select central zone and lowers it to 1/N
central_zone = oldmask(cm_coord(1)-rad_px:cm_coord(1)+rad_px,cm_coord(2)-rad_px:cm_coord(2)+rad_px)*1/N;

% % plot central zone (for debug)
% figure
% imagesc(central_zone)
% colorbar

% create newmask
newmask = oldmask;
newmask(cm_coord(1)-rad_px:cm_coord(1)+rad_px,cm_coord(2)-rad_px:cm_coord(2)+rad_px) = central_zone;

% % plot newmask adjusted (for debug)
% figure
% imagesc(newmask)
% colorbar
return