
% ppr = [
%     500 490
%     526 491
%     498 530
%     525 532
%     ];

ppr = [
    492 493
    511 468
    494 467
    510 496
    ];


dir1 = atan2(abs(ppr(3,1)-ppr(1,1)),abs(ppr(3,2)-ppr(1,2)));

dir2 = atan2(abs(ppr(4,1)-ppr(1,1)),abs(ppr(4,2)-ppr(1,2)));

x0 = ppr(1,:);

xx = 450:size(final_image_ap,1)-450;
y = round(dir1.*(xx-x0(1))+x0(2));

ind = sub2ind(size(final_image_ap),y,xx);
view_direction_of_plot(xx,y,final_image_ap);

[pks,locs] = findpeaks(final_image_ap(ind),xx,'MinPeakHeight',4e4);

view_direction_of_plot(xx,y,sun);

figure
plot(xx,sun(ind)/max(sun(ind)))
hold on
plot(xx,final_image_ap(ind)/max(final_image_ap(ind)))