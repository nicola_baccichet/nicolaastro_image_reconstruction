function [offsets,modPSF] = align_PSF(currentPSF,starting_image,direction_image)
% MTF-real image alignment script
% author: Nicola Baccichet, nicola.baccichet.12@ucl.ac.uk
% version: 2.1
% last modified: 26/06/2015
%%
OTF = fft2(fftshift(currentPSF));
MTF = ifftshift(abs(OTF/OTF(1,1)));

% get FT of image
ft_im = abs(ifftshift(fft2(fftshift(starting_image))));
ft_im2 = abs(ifftshift(fft2(fftshift(direction_image))));

scrz = get(0,'screensize'); % get data on screen size
warning('off','images:initSize:adjustingMag')   % clear from warning messages
warning('off','images:truesize:imageTooBigForScreen')
% plot mtf and image for comparison
h = figure;
subplot(1,2,1), imagesc(MTF); title('uv points'),axis square;
hold on, plot([0,size(MTF,1)],[size(MTF,1)/2,size(MTF,2)/2]),plot([size(MTF,1)/2,size(MTF,2)/2],[0,size(MTF,2)])
subplot(1,2,2), imagesc(log(ft_im)); title('image fft'),axis square
truesize(gcf)
%% flip check
flchoice = questdlg('Flip uv points?', ...
        'Flip Menu', ...
        'Yes','No','No');
if strcmp(flchoice,'Yes')==1
    close(h)
    modPSF = fliplr(currentPSF);
    OTF = fft2(fftshift(modPSF));
    MTF = ifftshift(abs(OTF/OTF(1,1)));
    % plot again
    h = figure;
    subplot(1,2,1), imagesc(MTF); title('uv points'),axis square;
%     hold on, plot([0,1024],[1024/2,1024/2]),plot([1024/2,1024/2],[0,1024])
    hold on, plot([0,size(MTF,1)],[size(MTF,1)/2+1,size(MTF,2)/2+1]),plot([size(MTF,1)/2+1,size(MTF,2)/2+1],[0,size(MTF,2)])
    subplot(1,2,2), imagesc(log(ft_im)); title('image fft'),axis square
    truesize(gcf)
else
    modPSF = currentPSF;
end


%% rotation check - old
% rotation_flag = 1;    % flag used in rotation procedure
% 
% while rotation_flag == 1
%     % Construct a questdlg with two options
%     choice = questdlg('What would you like to do?', ...
%         'Rotation Menu', ...
%         'Rotate uv points','Nothing','Nothing');
%     close(h)
%     % Handle response
%     switch choice
%         case 'Rotate uv points'
%             rotation_flag = 1;
%             
%             prompt = {'Enter rotation angle (counterclockwise, in degrees):'};
%             dlg_title = 'Input';
%             num_lines = 1;
%             def = {'0'};
%             answer = inputdlg(prompt,dlg_title,num_lines,def);
%             % rotate psf by user input angle
%             rotatedPSF = imrotate(currentPSF,str2double(answer),'bilinear','crop');
%             OTF = fft2(fftshift(rotatedPSF));
%             MTF = ifftshift(abs(OTF/OTF(1,1)));
%             % generate plot for comparison
%             h = figure;
%             subplot(1,2,1), imagesc(MTF); title('uv points'),axis square
%             hold on, plot([0,1024],[1024/2,1024/2]),plot([1024/2,1024/2],[0,1024])
%             subplot(1,2,2), imagesc(ft_im); title('image fft'),axis square           
%             disp(['MTF rotated counterclockwise by ' answer ' degrees'])
%             truesize(gcf)
%             % set new spf
%             newPSF = rotatedPSF;
%         case 'Nothing'
%             newPSF = currentPSF;
%             disp('returning to main program')
%             rotation_flag = 2;
%     end
% end
% close all
%% angles alignment procedure
rotation_flag = 1;    % flag used in rotation procedure
close(h)

c = size(MTF)/2+[1 1];  % images center
answ_data = 0;  % starting angle for first real image
answ_mtf = 0;   % starting angle for computed mtf
answ_data2 = 0; % starting angle for second real image

while rotation_flag == 1
    % Construct a questdlg with two options
    choice = questdlg('What would you like to do? (choose in sequence)', ...
        'Alignment Menu', ...
        'Align data (1)','Align second set of data (2)','Align uv points and exit (3)',...
        'Align uv points and exit (3)');
%     close(h)
    % Handle response
    switch choice
        case 'Align data (1)'
            %% first image alignment
            rotation_flag = 1;
            a = -tan(deg2rad(answ_data));
            x = 300:size(ft_im,1)-300;
            y = round(a.*(x-c(1))+c(2));
            %plot data FFT
            h = view_direction_of_plot(x,y,log(ft_im)); truesize(gcf)
            % display selection window
            prompt = {'Enter rotation angle (counterclockwise, in degrees):'};
            dlg_title = 'Input';
            num_lines = 1;
            def = {'0'};
            answer = inputdlg(prompt,dlg_title,num_lines,def);
            close(h)
            % rotate psf by user input angle
            a = -tan(deg2rad(answ_data + str2double(answer)));
            x = 300:size(ft_im,1)-300;
            y = round(a.*(x-c(1))+c(2));
            % plot selected direction
            h = view_direction_of_plot(x,y,log(ft_im)); truesize(gcf)
            answ_data = answ_data + str2double(answer);
        case 'Align uv points and exit (3)'
            %% mtf alignment
%             rotation_flag = 1;
            close(h)
            h=figure;
            imagesc(MTF); title('uv points'),axis square, truesize(gcf);
            a = -tan(deg2rad(answ_mtf));
            x = 300:size(MTF,1)-300;   y = round(a.*(x-c(1))+c(2));
            hold on, plot(x,y)
            % display selection window
            prompt = {'Enter rotation angle (counterclockwise, in degrees):'};
            dlg_title = 'Input';
            num_lines = 1;
            def = {'0'};
            answer = inputdlg(prompt,dlg_title,num_lines,def); 
            close(h)
            % rotate mtf by user input angle
            a = -tan(deg2rad(answ_mtf + str2double(answer)));
            x = 300:size(MTF,1)-300;
            y = round(a.*(x-c(1))+c(2));
            % plot selected direction
            h = view_direction_of_plot(x,y,MTF); truesize(gcf)
            answ_mtf = answ_mtf + str2double(answer);
%             rotation_flag=2;
            prompt = {'Enter (1) to continue with rotation, (2) to exit:'};
            dlg_title = 'Input';   num_lines = 1;  def = {'2'};
            rotation_flag = str2double(inputdlg(prompt,dlg_title,num_lines,def));
        case 'Align second set of data (2)'
            %% second image alignment
            rotation_flag = 1;
            a = -tan(deg2rad(answ_data2));
            x = 300:size(ft_im2,1)-300;
            y = round(a.*(x-c(1))+c(2));
            %plot data FFT
            h = view_direction_of_plot(x,y,log(ft_im2)); truesize(gcf)
            % display selection window
            prompt = {'Enter rotation angle (counterclockwise, in degrees):'};
            dlg_title = 'Input';
            num_lines = 1;
            def = {'0'};
            answer = inputdlg(prompt,dlg_title,num_lines,def);
            close(h)
            % rotate psf by user input angle
            a = -tan(deg2rad(answ_data2 + str2double(answer)));
            x = 300:size(ft_im2,1)-300;
            y = round(a.*(x-c(1))+c(2));
            % plot selected direction
            h = view_direction_of_plot(x,y,log(ft_im2)); truesize(gcf)
            answ_data2 = answ_data2 + str2double(answer);
    end
end
%% Run angle optimization (4)
            close all
            disp(['angles selected by user (mtf,data1,data2): ' num2str([answ_mtf ,answ_data, answ_data2])])
            %% run search for zero angle on image
            zero_ang_image = findZeroMTF(ft_im,answ_data);
%             zero_ang_image = answ_data;
            a = -tan(deg2rad(zero_ang_image));
            x = 300:size(ft_im,1)-300;
            y = round(a.*(x-c(1))+c(2));
            view_direction_of_plot(x,y,log(ft_im)); truesize(gcf)
            %% run search for zero angle on MTF
            zero_ang_mtf = findZeroMTF(MTF,answ_mtf);
%             zero_ang_mtf = answ_mtf;
            a = -tan(deg2rad(zero_ang_mtf));
            x = 300:size(MTF,1)-300;
            y = round(a.*(x-c(1))+c(2));            
            view_direction_of_plot(x,y,MTF); truesize(gcf)
            %% run search for zero angle on second image
            zero_ang_image2 = findZeroMTF(ft_im2,answ_data2);
%             zero_ang_image2 = answ_data2;
            a = -tan(deg2rad(zero_ang_image2));
            x = 300:size(ft_im,1)-300;
            y = round(a.*(x-c(1))+c(2));
            view_direction_of_plot(x,y,log(ft_im2)); truesize(gcf)  
            disp(['optimized angles (mtf,data1,data2): ' num2str([zero_ang_mtf ,zero_ang_image,zero_ang_image2])])            
            %
            disp('angles optimized')
%% return rotation angle offsets
offsets = [zero_ang_mtf ,zero_ang_image,zero_ang_image2];
return