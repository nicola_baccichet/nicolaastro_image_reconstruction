function mask = create_mask_withW(c,r,w,sizearray)

c = (c); %+repmat([0 1],43,1);
r = median(r);

[X,Y]=meshgrid(1:sizearray(1),1:sizearray(2));

for i=1:size(c,1)
    mask_single = (X-c(i,1)).^2+(Y-c(i,2)).^2<=r.^2;
    mask_tot(:,:,i) = mask_single*w(i);
end
mask = sum(mask_tot,3);
return