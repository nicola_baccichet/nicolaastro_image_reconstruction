function [metric_im,c_im,r_im]=get_weigths(current_image,c_uv)

ftimage_current = fft2(fftshift(current_image));
ftimage_current = ifftshift(ftimage_current/ftimage_current(1,1));
ftimage_current = log(abs(ftimage_current));

%% find circles in image
sensitivity = 0.935; edgethreshold = 0.21;
[c_im, r_im, metric_im] = imfindcircles(ftimage_current,[3 7],...
    'Sensitivity',sensitivity,'Method','TwoStage','EdgeThreshold',edgethreshold);
%% plot first iteration
% figure
% imagesc(ftimage_current);axis square, truesize(gcf)
% hold on
% viscircles(c_im,r_im,'EdgeColor','r');
% % plot(c_im(:,1),c_im(:,2),'o')
% title(['sensitivity=' num2str(sensitivity) ' edge threshold=' num2str(edgethreshold)])
%% select points corresponding to (u,v) coords only
[~,I] = pdist2(c_im,c_uv,'euclidean','Smallest',1);
c_im = c_im(I,:); 
r_im = r_im(I);
metric_im = metric_im(I);
%% normalize metric
metric_im = metric_im/max(metric_im);
%% plot second iteration
% figure
% imagesc(ftimage_current);axis square, truesize(gcf)
% hold on
% viscircles(c_im,r_im,'EdgeColor','r');
% % plot(c_im(:,1),c_im(:,2),'o')
% title(['sensitivity=' num2str(sensitivity) ' edge threshold=' num2str(edgethreshold)])