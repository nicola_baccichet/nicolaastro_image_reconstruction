% function uv_points_analysis(mtf,ftimage)
% create table of coordinates of points in MTF and real image
% in other words, provides the visibilites
% author: Nicola Baccichet, nicola.baccichet.12@ucl.ac.uk
% version: beta.2.1
% last modified: 05/11/2015
warning('off','images:imfindcircles:warnForSmallRadius')   % clear from warning messages of imfindcircles
warning('off','images:initSize:adjustingMag')   % clear from warning messages of resizing
clear all; close all; clc;

% WARNING: SOME POINTS MAY BE INVERTED IN POSITION. NEED TO CHECK AFTER IN
% SPREADSHEET

% uiwait(msgbox('Select the .mat file containing the image reconstruction data'));  % not working for version older than 2015
% [filename,filepath]=uigetfile('*.mat','Select the .mat file containing the image reconstruction data');
filename = 'half_rot.mat'; 
filepath = '/media/ucapacc/DATAPART1/PHD/Interferometers study/LAM/image_reconstruction/RECONSTRUCTION_RESULTS/09-Nov-2015/';
load([filepath filename],'all_images','mtf_at_different_angles','xang','x','s','px');
%%
for i=1:size(all_images,3)
% for i=1:1

current_image = i;

ftimage_current =  (fft2(fftshift(all_images(:,:,current_image))));
ftimage_current = ifftshift(ftimage_current/ftimage_current(1,1));
mtf_current = mtf_at_different_angles(:,:,current_image);

sav = 1;    % saving flag, put to 1 to save txt, 2 to save xls
%% find circles in MTF
[c_uv, r_uv,metric_uv] = imfindcircles(mtf_current,[3 7],...
    'Sensitivity',0.99,'Method','TwoStage','EdgeThreshold',0.09);
% sort data in ascending order
[nc_uv,idx] = sortrows(c_uv,1);
metric_uv = metric_uv(idx); 
% % plot
% figure
% imagesc(mtf_current);axis square, truesize(gcf)
% hold on
% viscircles(c_uv,r_uv,'EdgeColor','r');
% title('MTF with detected circles')
%% find circles in image
sensitivity = 0.935; edgethreshold = 0.21;
[c_im, r_im, metric_im] = imfindcircles(log(abs(ftimage_current)),[3 7],...
    'Sensitivity',sensitivity,'Method','TwoStage','EdgeThreshold',edgethreshold);

% % plot
% figure
% imagesc(log(abs(ftimage_current)));axis square, truesize(gcf)
% hold on
% viscircles(c_im,r_im,'EdgeColor','r');
% % plot(c_im(:,1),c_im(:,2),'o')
% title(['sensitivity=' num2str(sensitivity) ' edge threshold=' num2str(edgethreshold)])
%% select points of (u,v) coords only
[~,I] = pdist2(c_im,c_uv,'euclidean','Smallest',1);
c_im = c_im(I,:); r_im = r_im(I);

[nc_im,idx2] = sortrows(c_im,1);
metric_im = metric_im(idx2);

% figure
% imagesc(log(abs(ftimage_current)));axis square, truesize(gcf)
% hold on
% viscircles(c_im,r_im,'EdgeColor','r');
% % plot(c_im(:,1),c_im(:,2),'o')
% title(['sensitivity=' num2str(sensitivity) ' edge threshold=' num2str(edgethreshold)])

%% create polar list of coordinates for centers

% center cartesian coordinates in (513,513)
x0 = [513, 513];
nc_im = nc_im - repmat(x0,size(nc_im,1),1);
nc_uv = nc_uv - repmat(x0,size(nc_uv,1),1);

[thpol_im, rpol_im] = cart2pol(nc_im(:,1),nc_im(:,2));
[thpol_uv, rpol_uv] = cart2pol(nc_uv(:,1),nc_uv(:,2));

%% save excel table with coordinates
if sav ==2
xlsxname = [filename(1:end-4) '.xlsx'];

% write uv coordinates
datatitles = {'x_uv','y_uv','r_uv','theta_uv','x_im','y_im','r_im','theta_im','Re(image)','Im(image)'};
xlRange = 'A1';
xlswrite([filepath xlsxname],datatitles,current_image,xlRange);

% write uv coordinates
uvdata = [nc_uv(:,1),nc_uv(:,2),rpol_uv,thpol_uv];
xlRange = 'A2';
xlswrite([filepath xlsxname],uvdata,current_image,xlRange);

% write im coordinates
imdata = [nc_im(:,1),nc_im(:,2),rpol_im,thpol_im];
xlRange = 'E2';
xlswrite([filepath xlsxname],imdata,current_image,xlRange);
end
%% get bias

biaspath = '/media/ucapacc/DATAPART1/PHD/Interferometers study/LAM/data/sun/09 june 2015/bias/';
bias_list = dir([biaspath '/*.TIF']);
for i = 1:length(bias_list)
    all_bias(:,:,i)= double( imread([biaspath '/' bias_list(i).name]));
end
Bias = mean(all_bias,3);

%% get real and imaginary parts values
% create mesh to define the circles
[imageSizeX,imageSizeY] = size(ftimage_current);
[columnsInImage,rowsInImage] = meshgrid(1:imageSizeX, 1:imageSizeY);
ftbias = real(ifftshift(fft2(fftshift(Bias))));

% create matrix of real and imaginary values
for i=1:size(r_im)
    % define center coordinates and radius
    centerX = fix(nc_im(i,1)+513);
    centerY = fix(nc_im(i,2)+513);
    radius = r_uv(1)+1;
    % create circle at XY and radius - values are zeros and ones
    circlePixels = (rowsInImage - centerY).^2 + (columnsInImage - centerX).^2 <= radius.^2;
    % get corresponding values in the ftimage
    complexvalues = (ftimage_current(circlePixels==1));
    biasvalues = ftbias(circlePixels==1);
    % compute average
    realmean(i) = mean(real(complexvalues));
    imagmean(i) = mean(imag(complexvalues));
    realstd(i) = std(real(complexvalues));
    imagstd(i) = std(imag(complexvalues));
end

if sav==2
% save Re and Im in a spreadsheet tab
imReIm = [realmean',imagmean'];
xlRange = 'I2';
xlswrite([filepath xlsxname],imReIm,current_image,xlRange);
end

if sav==1
% save everything in txt
name = [filepath 'complex_vis_' num2str(current_image) '.txt'];
tosave = [realmean',imagmean',rpol_im,metric_im,realstd',imagstd'];
save(name,'tosave','-ascii','-double','-tabs');
end


end


%% bessel analysis - coarse - Wittkowski2001

% get complex intensity of uv points
complex_intensity = (realmean.^2+imagmean.^2);
% complex_intensity(22) = complex_intensity(22)/(49*3.2);
complex_intensity = complex_intensity/complex_intensity(22);    % normalize by the peak
lambda = 0.61; % um

P = 1024*px*s;          % length of ccd in arcsec
df = 1/P;                      % scale unit of the FT of image in arcsec
dth = px*s;        % scale unit for ccd in arcsec
fth = -1/(2*dth) : 1/P : 1/(2*dth)-1/P;     % coordinates grid in uv plane (cyc/arcsec)

rpol_im_real = rpol_im*df;    % convert to length in - cyc/arcsec

% set bessel equation for uniform disk
r = 1e-6:.00001:max(rpol_im_real);  % in (u,v) plane - cyc/arcsec
Rsun = 0.0006;                         % angular diam
Z = @(Rsun) pi*r./Rsun;
% compute bessel function
bsun = abs(2*besselj(1,Z(Rsun))./Z(Rsun)).^2;
% bsun(1) = 1;

figure,
plot(rpol_im_real,complex_intensity,'o')
hold on
plot(r,bsun,'r')
% ylim([0 0.03])

%% bessel analysis from Wittkowski2001 - optimization
% options = optimoptions('lsqcurvefit','TolFun',1e-30,'TolX',1e-30,'MaxFunEvals',1e4,'MaxIter',1e4);
% stpUD = 0.02
% vUD =@(th,r) abs(2*besselj(1,pi*th.*r)./(pi*th.*r)).^2;
% [thUD,resnormUD,residualsUD]=lsqcurvefit(vUD,stpUD,rpol_im_real,complex_intensity',0,2000,options);
% 
% % figure,plot(r,vUD(thUD,r)); hold on; plot(rpol_im_real,complex_intensity,'o')   
% % ylim([0 0.02]); title('uniform disk')
% 
% stpFDD = thUD;
% vFDD=@(th,r) abs(3*sqrt(pi)*besselj(3/2,pi*th.*r)./(sqrt(2)*(pi*th.*r).^(3/2))).^2;
% [thFDD,resnormFDD,residualsFDD]=lsqcurvefit(vFDD,stpFDD,rpol_im_real,complex_intensity',0,2000,options);
% 
% figure,plot(r,vFDD(thFDD,r)); hold on; plot(rpol_im_real,complex_intensity,'o')
% ylim([0 0.1]); title('fully darkened disk')

%% weighting analysis

% ap = 8.4;   % um - single aperture diameter

figure
plot((c_im(:,1)-513)*df,(c_im(:,2)-513)*df,'k+'), axis square;
% hold on; plot((c_uv(:,1)-513)*df,(c_uv(:,2)-513)*df,'o'), axis square;
axis([-1/(2*dth) 1/(2*dth)-1/P -1/(2*dth) 1/(2*dth)-1/P])
xlabel('u [cyc/arcsec]'), ylabel('v [cyc/arcsec]')
 
% % plot grid
% hold on
% int = [-1/(2*dth): 0.5*ap : 1/(2*dth)-1/P];
% for i=1:length(int)
%     plot([int(i),int(i)],[int(1),int(end)],'k','LineWidth',0.001,'LineStyle','-')
%     plot([int(1),int(end)],[int(i),int(i)],'k','LineWidth',0.001,'LineStyle','-')
% end
