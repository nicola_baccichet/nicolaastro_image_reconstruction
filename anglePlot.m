function values = anglePlot(center_position,angle,image_to_plot)
% plot desired line of the image, centered to a point and with respect to a
% certain angle with the x axis
% the specific data are returned as output
% author: Nicola Baccichet, nicola.baccichet.12@ucl.ac.uk
% version: 1
% last modified: 02/06/2015

% center_position = [513;513];  % image center

a = -tan(deg2rad(angle));
x = 300:size(image_to_plot,1)-300;
y = round(a.*(x-center_position(1))+center_position(2));

% plot along defined line
ind = sub2ind(size(image_to_plot),y,x);

view_direction_of_plot(x,y,image_to_plot);
truesize(gcf)

values = image_to_plot(ind);
figure,plot(x,values)

