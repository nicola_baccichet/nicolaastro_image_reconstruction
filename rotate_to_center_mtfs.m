function [rmtf2,thdiff] = rotate_to_center_mtfs(PSF,scienceImage,sgn)
% automatic MTF rotation script, optimized for the 7-apertures case @ LAM
% might work even with other interferometers, with some tweaks
% author: Nicola Baccichet, nicola.baccichet.12@ucl.ac.uk
% version: beta.1
% last modified: 26/10/2015
%%
% test picture
% scienceImage = all_images(:,:,3); close all; clc

% central area limits
p1 = [450,450];
p2 = [577,577];

% crop MTF to central area
OTF = fft2(fftshift(PSF));
mtf = ifftshift(abs(OTF/OTF(1,1)));       clear OTF
rmtf = mtf(p1(1):p2(1),p1(2):p2(2));

% crop image to central area
scienceImage = log(abs(ifftshift(fft2(fftshift(scienceImage)))));
rft = scienceImage(p1(1):p2(1),p1(2):p2(2));

% find circles on mtf
sens = 0.99;
% thre = 0.1;     % for 0-180 deg
thre = 0.08;    % for 0-360 deg
[c_uv, r_uv] = imfindcircles(rmtf,[3 7],'Sensitivity',sens,'Method','TwoStage','EdgeThreshold',thre);
% figure, imagesc(rmtf);
% hold on; viscircles(c_uv,r_uv,'EdgeColor','r');

%% find circles on image
sens1 = 0.9;
thre1 = 0.259;
[c_im, r_im] = imfindcircles(rft,[3 6],'Sensitivity',sens1,'Method','TwoStage','EdgeThreshold',thre1);

c_im = [c_im(r_im<5,1) c_im(r_im<5,2)];
r_im = r_im(r_im<5);
%     
if size(c_im,1)~= 7
%     sens1 = 0.91;   % for 0-180 deg
    sens1 = 0.921;  % for 0-360 deg
    thre1 = 0.269;
    [c_im, r_im] = imfindcircles(rft,[3 6],'Sensitivity',sens1,'Method','TwoStage','EdgeThreshold',thre1);

    c_im = [c_im(r_im<5,1) c_im(r_im<5,2)];
    r_im = r_im(r_im<5);
end
% figure, imagesc(rft);
% hold on; viscircles(c_im,r_im,'EdgeColor','r');
%% alignment
% center cartesian coordinates
c = [64,64];
c_im = c_im - repmat(c,size(c_im,1),1);
c_uv = c_uv - repmat(c,size(c_uv,1),1);

% remove central point
c_im(1,:) = []; r_im(1) =[];
c_uv(1,:) = []; r_uv(1) =[];

% sort series by points
c_im2 = sortrows(c_im,1); 
c_uv2 = (sortrows(c_uv,2));

% convert into polar coordinates
[thpol_im, rpol_im] = cart2pol(c_im2(:,1),c_im2(:,2));
[thpol_uv, rpol_uv] = cart2pol(c_uv2(:,1),c_uv2(:,2));
% figure, plot(c_uv2(:,1),c_uv2(:,2),'ko'),hold on, axis square
% plot(c_im2(:,1),c_im2(:,2),'ro')
% text(c_im2(:,1),c_im2(:,2),num2str(rpol_im))
% text(c_uv2(:,1),c_uv2(:,2),num2str(rpol_uv))

% sort by radius
[rpol_im,ind_im]=sortrows(rpol_im); thpol_im = thpol_im(ind_im);
[rpol_uv,ind_uv]=sortrows(rpol_uv); thpol_uv = thpol_uv(ind_uv);

% make paris right (im)
[thpol_im(1:2),ii]=sortrows(thpol_im(1:2)); rpol_im(1:2) = rpol_im(ii);
[thpol_im(3:4),jj]=sortrows(thpol_im(3:4)); rpol_im(3:4) = rpol_im(jj+2);
[thpol_im(5:6),kk]=sortrows(thpol_im(5:6)); rpol_im(5:6) = rpol_im(kk+4);

% make paris right (uv)
[thpol_uv(1:2),ii]=sortrows(thpol_uv(1:2)); rpol_uv(1:2) = rpol_uv(ii);
[thpol_uv(3:4),jj]=sortrows(thpol_uv(3:4)); rpol_uv(3:4) = rpol_uv(jj+2);
[thpol_uv(5:6),kk]=sortrows(thpol_uv(5:6)); rpol_uv(5:6) = rpol_uv(kk+4);

thdiff = (thpol_uv-thpol_im);
thdiff = rad2deg(mode(thdiff(1)))

PSF = imrotate(PSF,sgn*thdiff,'nearest','crop');
OTF = fft2(fftshift(PSF));
rmtf2 = ifftshift(abs(OTF/OTF(1,1)));       clear OTF

% figure, imagesc(rmtf2(p1(1):p2(1),p1(2):p2(2)));